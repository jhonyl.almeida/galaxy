package mlexam.core.model.weather;

import org.bson.codecs.pojo.annotations.BsonId;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.mongodb.panache.MongoEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@MongoEntity(collection="day")
public class WeatherDay {
	@BsonId
	@JsonIgnore
	private String id;
	private String jobId;
	private int day;
	private Weather weather;
	private boolean rainPeak;

	public static WeatherDay of(String jobId, int day, Weather weather, boolean rainPeak) {
		return new WeatherDay(jobId + "-" + day, jobId, day, weather, rainPeak);
	}
}

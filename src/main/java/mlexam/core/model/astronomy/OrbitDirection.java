package mlexam.core.model.astronomy;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum OrbitDirection {
	CLOCKWISE(1.0),
	COUNTER_CLOCKWISE(-1.0);

	double multiplier;
}

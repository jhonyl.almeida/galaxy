package mlexam.core.model.astronomy;

public interface PlanetSpecification {
	String getName();
	double getKilometersFromSun();
	double getDegreesPerDay();
	OrbitDirection getOrbitDirection();
}

package mlexam.web.controller;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

import mlexam.web.dto.PostJobRequest;
import mlexam.web.dto.PostJobResponse;
import mlexam.core.model.weather.Weather;
import mlexam.core.model.weather.WeatherDay;
import mlexam.core.model.weather.WeatherSummary;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@QuarkusTest
@QuarkusTestResource(TestContainersLifecycle.class)
class WeatherControllerIT {

	private PostJobResponse postJobResponse;

	@BeforeEach
	public void setup() throws InterruptedException {
		postJobResponse = postSimulationJob();
		TimeUnit.SECONDS.sleep(1);
	}

    @Test
    void shouldReturnSummary() {
        final var summary = given()
				.when().get(String.format("/weather/summary/1", postJobResponse.getJobId()))
				.then()
				.statusCode(200)
				.extract().body().as(WeatherSummary.class);

		assertThat(summary.getJobId(), is(postJobResponse.getJobId()));
		assertThat(summary.getYears(), is(1));
		assertThat(summary.getDryPeriods(), is(3L));
		assertThat(summary.getRainPeriods(), is(116L));
		assertThat(summary.getOptimalConditionPeriods(), is(2L));
		assertThat(summary.getRainPeakDays(), hasItems(73, 109, 253, 289));
    }

    @Test
	void shouldReturnDayWeather() {
		final var day = given()
				.when().get(String.format("/weather/days/%d", 109))
				.then()
				.statusCode(200)
				.extract().body().as(WeatherDay.class);

		assertThat(day.getJobId(), is(postJobResponse.getJobId()));
		assertThat(day.getDay(), is(109));
		assertThat(day.getWeather(), is(Weather.RAINY));
		assertThat(day.isRainPeak(), is(true));
	}

	private PostJobResponse postSimulationJob() {
		return given()
				.when()
				.body(PostJobRequest.of(1, null))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.post("/weather")
				.then()
				.statusCode(200)
				.extract().body().as(PostJobResponse.class);
	}

}
package mlexam.core.service;

import mlexam.core.model.astronomy.SolarSystemSimulationJob;

public interface SimulationQueueService {

	void send(SolarSystemSimulationJob simulation);

}

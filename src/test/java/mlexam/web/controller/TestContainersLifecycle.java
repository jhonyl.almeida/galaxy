package mlexam.web.controller;

import java.util.Collections;
import java.util.Map;

import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

public class TestContainersLifecycle implements QuarkusTestResourceLifecycleManager {

	@Container
	public final static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.4.4"));

	@Override
	public Map<String, String> start() {
		mongoDBContainer.start();
		final var mongoUri = String.format("mongodb://%s:%d/?retryWrites=true&w=majority",
				mongoDBContainer.getContainerIpAddress(),
				mongoDBContainer.getMappedPort(27017));

		return Collections.singletonMap("quarkus.mongodb.connection-string", mongoUri);
	}

	@Override
	public void stop() {
		mongoDBContainer.close();
	}

}
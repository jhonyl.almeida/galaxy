package mlexam.core.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import lombok.extern.slf4j.Slf4j;
import mlexam.core.model.astronomy.SolarSystemSimulationJob;
import mlexam.core.repository.WeatherDayRepository;
import mlexam.core.repository.WeatherSummaryRepository;

@ApplicationScoped
@Slf4j
public class ExecutorSimulationQueueService implements SimulationQueueService {

	@Inject
	WeatherSummaryRepository summaryRepository;

	@Inject
	WeatherDayRepository dayRepository;

	private final ExecutorService jobExecutor = Executors.newSingleThreadExecutor();

	public void send(SolarSystemSimulationJob simulation) {
		final var jobId = simulation.getJobId();

		CompletableFuture
				.supplyAsync(simulation::execute, jobExecutor)
				.whenCompleteAsync((result, throwable) -> {
					if (throwable != null) {
						log.error("An error occurred while executing job id {}", jobId, throwable);
						summaryRepository.persistOrUpdate(result.getSummary()).subscribeAsCompletionStage();
						return;
					}

					try {
						dayRepository.persistOrUpdate(result.getCalendar()).subscribeAsCompletionStage();
						summaryRepository.persistOrUpdate(result.getSummary()).subscribeAsCompletionStage();
					} catch (Exception exception) {
						log.error("Could not store summary for job id {}", jobId, exception);
					}
				}, jobExecutor);
	}

}

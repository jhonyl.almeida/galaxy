package mlexam.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mlexam.core.model.astronomy.OrbitDirection;
import mlexam.core.model.astronomy.PlanetSpecification;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PlanetDto implements PlanetSpecification {
	private String name;
	private double kilometersFromSun;
	private double degreesPerDay;
	private OrbitDirection orbitDirection;
}

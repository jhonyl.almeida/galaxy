package mlexam.web.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.smallrye.mutiny.Uni;

import lombok.extern.slf4j.Slf4j;

import mlexam.core.service.SimulationQueueService;
import mlexam.web.dto.PostJobRequest;
import mlexam.web.dto.PostJobResponse;
import mlexam.core.model.astronomy.OrbitDirection;
import mlexam.core.model.astronomy.Planet;
import mlexam.core.model.astronomy.SolarSystemSimulationJob;
import mlexam.core.repository.WeatherDayRepository;
import mlexam.core.repository.WeatherSummaryRepository;
import mlexam.core.model.weather.WeatherDay;
import mlexam.core.model.weather.WeatherSummary;

@ApplicationScoped
@Slf4j
public class WeatherService {

	private static final List<Planet> DEFAULT_PLANETS = List.of(
			Planet.of("Ferengi",  500.0, 1.0, OrbitDirection.CLOCKWISE),
			Planet.of("Vulcano",  1000.0, 5.0, OrbitDirection.COUNTER_CLOCKWISE),
			Planet.of("Betasoide",  2000.0, 3.0, OrbitDirection.CLOCKWISE));

	@Inject
	SimulationQueueService simulationQueueService;

	@Inject
	WeatherSummaryRepository summaryRepository;

	@Inject
	WeatherDayRepository dayRepository;

	public Uni<WeatherSummary> testDefaultScenario(int years) {
		return Uni.createFrom().item(runSyncSimulation(years));
	}

	public Uni<WeatherDay> findDailyWeather(int day, Optional<String> jobId) {
		if (jobId.isEmpty()) {
			return dayRepository.findFirstByDay(day);
		}

		return dayRepository.findByJobAndDay(jobId.get(), day);
	}

	public Uni<WeatherSummary> findSummary(int years, Optional<String> jobId) {
		if (jobId.isEmpty()) {
			return summaryRepository.findFirstByYears(years);
		}

		return summaryRepository.findByJobAndYears(jobId.get(), years);
	}

	public PostJobResponse sendSimulationJob(PostJobRequest request) {
		final var planets = Optional.ofNullable(request.getPlanets())
				.map(planetDtos -> planetDtos.stream().map(Planet::of).collect(Collectors.toList()))
				.orElse(DEFAULT_PLANETS);
		final var simulation = new SolarSystemSimulationJob(planets, request.getYears());
		simulationQueueService.send(simulation);

		return PostJobResponse.of(simulation.getJobId());
	}

	private WeatherSummary runSyncSimulation(int years) {
		final var solarSystem = new SolarSystemSimulationJob(DEFAULT_PLANETS, years);
		solarSystem.execute();

		return solarSystem.getSummary();
	}

}

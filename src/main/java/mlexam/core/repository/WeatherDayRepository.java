package mlexam.core.repository;

import javax.enterprise.context.ApplicationScoped;

import org.bson.Document;

import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepositoryBase;
import io.smallrye.mutiny.Uni;

import mlexam.core.model.weather.WeatherDay;

@ApplicationScoped
public class WeatherDayRepository implements ReactivePanacheMongoRepositoryBase<WeatherDay, Long> {

	public Uni<WeatherDay> findFirstByDay(int day) {
		return find(new Document("day", day)).firstResult();
	}

	public Uni<WeatherDay> findByJobAndDay(String jobId, int day) {
		final var query = new Document();
		query.append("jobId", jobId);
		query.append("day", day);

		return find(query).firstResult();
	}

}

package mlexam.web.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PostJobRequest {
	private int years = 10;
	private List<PlanetDto> planets;
}

package mlexam.core.model.geometry;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateXY;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

public class Triangle {
	private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();

	private final Point p1;
	private final Point p2;
	private final Point p3;
	private final Polygon polygon;

	public Triangle(Point p1, Point p2, Point p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;

		polygon = GEOMETRY_FACTORY.createPolygon(new Coordinate[] {
				new CoordinateXY(p1.x, p1.y),
				new CoordinateXY(p2.x, p2.y),
				new CoordinateXY(p3.x, p3.y),
				new CoordinateXY(p1.x, p1.y)});
	}

	public boolean contains(Point point) {
		return GEOMETRY_FACTORY.createPoint(new CoordinateXY(point.getX(), point.getY())).within(polygon);
	}

	public boolean containsByBarycentric(Point point) {
		final double s = p1.getY() * p3.x - p1.x * p3.getY() + (p3.getY() - p1.getY()) * point.x + (p1.x - p3.x) * point.y;
		final double t = p1.x * p2.y - p1.y * p2.x + (p1.y - p2.y) * point.x + (p2.x - p1.x) * point.y;

		if ((s < 0) != (t < 0)) {
			return false;
		}

		final double A = -p2.y * p3.x + p1.y * (p3.x - p2.x) + p1.x * (p2.y - p3.y) + p2.x * p3.y;

		return A < 0
				? (s <= 0 && s + t >= A)
				: (s >= 0 && s + t <= A);
	}

	public static Triangle of(Point p1, Point p2, Point p3) {
		return new Triangle(p1, p2, p3);
	}
}

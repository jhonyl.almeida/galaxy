package mlexam.core.repository;

import javax.enterprise.context.ApplicationScoped;

import org.bson.Document;

import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepositoryBase;

import io.smallrye.mutiny.Uni;

import mlexam.core.model.weather.WeatherSummary;

@ApplicationScoped
public class WeatherSummaryRepository implements ReactivePanacheMongoRepositoryBase<WeatherSummary, String> {

	public Uni<WeatherSummary> findFirstByYears(int years) {
		return find(new Document("years", years)).firstResult();
	}

	public Uni<WeatherSummary> findByJobAndYears(String jobId, int years) {
		final var query = new Document();
		query.append("_id", jobId);
		query.append("years", years);

		return find(query).firstResult();
	}

}

package mlexam.web.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import mlexam.web.dto.ErrorDto;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		if (exception instanceof IllegalArgumentException) {
			return Response.status(Status.BAD_REQUEST)
					.entity(ErrorDto.of(exception.getMessage()))
					.build();
		}

		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(ErrorDto.of(exception.getMessage()))
				.build();
	}

}

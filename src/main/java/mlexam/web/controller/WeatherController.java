package mlexam.web.controller;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.smallrye.mutiny.Uni;

import mlexam.web.dto.PostJobRequest;
import mlexam.web.dto.PostJobResponse;
import mlexam.web.service.WeatherService;
import mlexam.core.model.weather.WeatherDay;
import mlexam.core.model.weather.WeatherSummary;

@Path("/weather")
public class WeatherController {

	private static final int DEFAULT_YEARS = 10;

	@Inject
	WeatherService weatherService;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public PostJobResponse postSimulationJob(PostJobRequest request) {
		return weatherService.sendSimulationJob(request);
	}

	@GET
	@Path("summary/{years}")
	@Produces(MediaType.APPLICATION_JSON)
	public Uni<WeatherSummary> getSummary(@PathParam("years") int years,
										  @QueryParam("jobId") Optional<String> jobId) {
		return weatherService.findSummary(years, jobId);
	}

	@GET
	@Path("/days/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Uni<WeatherDay> getDailyWeather(@PathParam("day") int day,
										   @QueryParam("jobId") Optional<String> jobId) {
		return weatherService.findDailyWeather(day, jobId);
	}

	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	public Uni<WeatherSummary> test(@QueryParam("years") Optional<Integer> years) {
		return weatherService.testDefaultScenario(years.orElse(DEFAULT_YEARS));
	}
}
package mlexam.core.model.astronomy;

import lombok.Getter;

import mlexam.core.model.geometry.Point;

@Getter
public class Planet extends AstronomicalBody {
	private final Point startPosition;
	private final double degreesPerDay;
	private final double orbitRadius;
	private final OrbitDirection orbitDirection;

	private double angleToSun;

	private Planet(String name, double kilometersFromSun, double degreesPerDay, OrbitDirection orbitDirection) {
		super(name, Point.of(0, kilometersFromSun));
		this.startPosition = Point.of(position.getX(), position.getY());
		this.degreesPerDay = degreesPerDay;
		this.orbitDirection = orbitDirection;
		this.orbitRadius = Math.sqrt(Math.pow(position.getX(), 2) + Math.pow(position.getY(), 2));
	}

	public boolean isOnStartPosition() {
		return startPosition.equals(position);
	}

	public void moveOneDay() {
		angleToSun += orbitDirection.multiplier * degreesPerDay % 360;
		position.move(angleToSun, orbitRadius);
	}

	public static Planet of(String name, double kilometersFromSun, double degreesPerDay, OrbitDirection direction) {
		return new Planet(name, kilometersFromSun, degreesPerDay, direction);
	}

	public static Planet of(PlanetSpecification specification) {
		return of(specification.getName(),
				specification.getKilometersFromSun(),
				specification.getDegreesPerDay(),
				specification.getOrbitDirection());
	}
}

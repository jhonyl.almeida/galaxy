package mlexam.core.model.weather;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Weather {
	NEUTRAL,
	DRY,
	RAINY,
	OPTIMAL
}

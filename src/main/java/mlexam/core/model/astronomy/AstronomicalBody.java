package mlexam.core.model.astronomy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import mlexam.core.model.geometry.Point;

@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class AstronomicalBody {
	protected final String name;
	protected Point position;

	public double getDistanceTo(AstronomicalBody astronomicalBody){
		return position.getDistanceTo(astronomicalBody.position);
	}

	public boolean isWithinTriangle(AstronomicalBody a, AstronomicalBody b, AstronomicalBody c) {
		return position.isWithinTriangle(a.position, b.position, c.position);
	}

	public static AstronomicalBody of(String name, double x, double y) {
		return new AstronomicalBody(name, Point.of(x, y));
	}
}

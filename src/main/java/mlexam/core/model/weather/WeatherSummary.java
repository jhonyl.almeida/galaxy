package mlexam.core.model.weather;

import java.util.List;
import java.util.Map;

import org.bson.codecs.pojo.annotations.BsonId;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.quarkus.mongodb.panache.MongoEntity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
@MongoEntity(collection="summary")
public class WeatherSummary {
	@BsonId
	private String jobId;
	private int years;
	private long dryPeriods;
	private long rainPeriods;
	private long optimalConditionPeriods;
	private List<Integer> rainPeakDays;
	private String error;

	private WeatherSummary(String jobId, int years, Map<Weather, Long> weatherCounts, List<Integer> rainPeakDays) {
		this.jobId = jobId;
		this.years = years;
		this.dryPeriods = weatherCounts.getOrDefault(Weather.DRY, 0L);
		this.rainPeriods = weatherCounts.getOrDefault(Weather.RAINY, 0L);
		this.optimalConditionPeriods = weatherCounts.getOrDefault(Weather.OPTIMAL, 0L);
		this.rainPeakDays = rainPeakDays;
	}

	public static WeatherSummary of(String jobId, int years, Map<Weather, Long> weatherCounts, List<Integer> rainPeakDays) {
		return new WeatherSummary(jobId, years, weatherCounts, rainPeakDays);
	}

	public static WeatherSummary ofError(String jobId, int years, String error) {
		final var summary = new WeatherSummary();
		summary.jobId = jobId;
		summary.years = years;
		summary.error = error;

		return summary;
	}
}

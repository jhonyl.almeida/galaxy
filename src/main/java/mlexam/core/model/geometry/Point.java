package mlexam.core.model.geometry;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(staticName = "of")
public class Point {
	private static final int CALCULATION_PRECISION = 9;

	double x;
	double y;

	public void move(double angle, double radius) {
		x = round(Math.sin(Math.toRadians(angle)) * radius);
		y = round(Math.cos(Math.toRadians(angle)) * radius);
	}

	public double getDistanceTo(Point point) {
		return Math.sqrt(Math.pow(point.x - this.x, 2) + Math.pow(point.y - this.y, 2));
	}

	public boolean isWithinTriangle(Point p1, Point p2, Point p3) {
		return Triangle.of(p1, p2, p3).contains(this);
	}

	private static double round(double value) {
		return BigDecimal.valueOf(value)
				.setScale(CALCULATION_PRECISION, RoundingMode.HALF_UP)
				.doubleValue();
	}
}

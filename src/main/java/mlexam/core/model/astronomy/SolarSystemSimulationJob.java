package mlexam.core.model.astronomy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import lombok.Getter;
import mlexam.core.model.weather.Weather;
import mlexam.core.model.weather.WeatherDay;
import mlexam.core.model.weather.WeatherSummary;
import mlexam.core.model.geometry.Line;

public class SolarSystemSimulationJob {

	@Getter
	private final String jobId;
	private final List<AstronomicalBody> solarSystemBodies;
	private final AstronomicalBody star;
	private final List<Planet> planets;

	private Map<Integer, Weather> cycleCalendar;
	private Set<Integer> cycleRainPeakDays;
	private int earthYears;
	private int currentCycleDay;
	private double maxTrianglePerimeter;

	@Getter
	private WeatherSummary summary;
	@Getter
	private List<WeatherDay> calendar;

	public SolarSystemSimulationJob(List<Planet> planets, int earthYears) {
		if (planets.size() != 3) {
			throw new IllegalArgumentException("Currently this system only supports a scenario with three planets");
		}

		jobId = String.valueOf(earthYears);
		this.earthYears = earthYears;
		this.planets = planets;
		star = AstronomicalBody.of("Sun", 0, 0);
		solarSystemBodies = new ArrayList<>();
		solarSystemBodies.add(star);
		solarSystemBodies.addAll(planets);
	}

	public SolarSystemSimulationJob execute() {
		try {
			initState(earthYears);
			generateCycleCalendar();
			calendar = buildCalendar(365 * earthYears);
			summary = buildSummary();
		} catch (Exception exception) {
			summary = WeatherSummary.ofError(jobId, earthYears, exception.getMessage());
		}

		return this;
	}

	private List<WeatherDay> buildCalendar(int earthDays) {
		final int cycleDays = cycleCalendar.size();

		return IntStream.rangeClosed(1, earthDays)
				.mapToObj(earthDay -> {
					final var cycleDay = earthDay % cycleDays != 0 ? earthDay % cycleDays : cycleDays;
					return WeatherDay.of(jobId, earthDay, cycleCalendar.get(cycleDay), cycleRainPeakDays.contains(cycleDay));
				})
				.collect(Collectors.toList());
	}

	private WeatherSummary buildSummary() {
		final var weatherCounts = buildWeatherCounts();
		final var rainPeakDays = buildRainPeakDays();

		return WeatherSummary.of(jobId, earthYears, weatherCounts, rainPeakDays);
	}

	private Map<Weather, Long> buildWeatherCounts() {
		return calendar.stream()
				.collect(Collectors.groupingBy(WeatherDay::getWeather, Collectors.counting()));
	}

	private List<Integer> buildRainPeakDays() {
		return calendar.stream()
				.filter(WeatherDay::isRainPeak)
				.map(WeatherDay::getDay)
				.sorted()
				.collect(Collectors.toList());
	}

	private void initState(int earthYears) {
		this.earthYears = earthYears;
		currentCycleDay = 0;
		maxTrianglePerimeter = 0;
		cycleCalendar = new HashMap<>();
		cycleRainPeakDays = new HashSet<>();
	}

	private void generateCycleCalendar() {
		while (currentCycleDay == 0 || !isCycleStart()) {
			cycleCalendar.put(++currentCycleDay, resolveDayWeather());
			planets.forEach(Planet::moveOneDay);
		}
	}

	private boolean isCycleStart() {
		return planets.stream().allMatch(Planet::isOnStartPosition);
	}

	private Weather resolveDayWeather() {
		if (areAligned(solarSystemBodies)) {
			return Weather.DRY;
		}

		if (areAligned(planets)) {
			return Weather.OPTIMAL;
		}

		if (star.isWithinTriangle(planets.get(0), planets.get(1), planets.get(2))) {
			updateRainPeakDays();
			return Weather.RAINY;
		}

		return Weather.NEUTRAL;
	}

	private void updateRainPeakDays() {
		final var trianglePerimeter = getTrianglePerimeter();

		if (trianglePerimeter > maxTrianglePerimeter) {
			maxTrianglePerimeter = trianglePerimeter;
			cycleRainPeakDays.clear();
		}

		if (trianglePerimeter == maxTrianglePerimeter) {
			cycleRainPeakDays.add(currentCycleDay);
		}
	}

	private double getTrianglePerimeter() {
		final var planet1 = planets.get(0);
		final var planet2 = planets.get(1);
		final var planet3 = planets.get(2);

		return planet1.getDistanceTo(planet2) + planet2.getDistanceTo(planet3) + planet1.getDistanceTo(planet3);
	}

	private boolean areAligned(List<? extends AstronomicalBody> astronomicalBodies) {
		return IntStream.range(0, astronomicalBodies.size() - 1)
				.mapToObj(i ->  Line.of(astronomicalBodies.get(i).position, astronomicalBodies.get(i + 1).position))
				.distinct()
				.count() == 1;
	}

}

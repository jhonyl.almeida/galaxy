package mlexam.core.model.geometry;

import lombok.Value;

@Value
public class Line {
	double slope;
	double intersection;

	private Line(Point a, Point b) {
		slope = (b.getY() - a.getY()) / (b.getX() - a.getX());
		intersection = a.getY() - (slope * a.getX());
	}

	public static Line of(Point a, Point b) {
		return new Line(a, b);
	}
}
